/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export const ApplicationInfoMock = {
    packageName: "[PC preview] unknown packageName",
    className: "[PC preview] unknown className",
    name: "[PC preview] unknown name",
    labelId: 1,
    iconId: 2,
    sourceDir: "[PC preview] unknown sourceDir",
    flags: 3,
    customizeData: {
        key: "[PC preview] unknown key",
        value: {
            CustomizeData: {
                name: "[PC preview] unknown name",
                value: "[PC preview] unknown value",
            }
        }
    }
}

export const WantMock = {
    deviceId: "[PC Preview] unknow deviceId",
    bundleName: "[PC Preview] unknow bundleName",
    abilityName: "[PC Preview] unknow abilityName",
    uri: "[PC Preview] unknow uri",
    type: "[PC Preview] unknow type",
    flag: "[PC Preview] unknow flag",
    action: "[PC Preview] unknow action",
    parameters: "[PC Preview] unknow parameters",
    entities: "[PC Preview] unknow entities"
 }

export const ShortcutInfoMock =  {
    id: "[PC preview] unknown id",
    bundleName: "[PC preview] unknown bundleName",
    hostAbility: "[PC preview] unknown hostAbility",
    icon: "[PC preview] unknown icon",
    label: "[PC preview] unknown label",
    disableMessage: "[PC preview] unknown disableMessage",
    wants: [
        {
            targetBundle: "[PC preview] unknown targetBundle",
            targetClass: "[PC preview] unknown targetClass",
        }],
    isStatic: "[PC preview] unknown isStatic",
    isHomeShortcut: "[PC preview] unknown isHomeShortcut",
    isEnabled: "[PC preview] unknown isEnabled",
}

export const ModuleUsageRecordMock = {
    bundleName: "[PC preview] unknown bundleName",
    appLabelId: "[PC preview] unknown appLabelId",
    name: "[PC preview] unknown name",
    labelId: "[PC preview] unknown labelId",
    descriptionId: "[PC preview] unknown descriptionId",
    abilityName: "[PC preview] unknown abilityName",
    abilityLabelId: "[PC preview] unknown abilityLabelId",
    abilityDescriptionId: "[PC preview] unknown abilityDescriptionId",
    abilityIconId: "[PC preview] unknown abilityIconId",
    launchedCount: "[PC preview] unknown launchedCount",
    lastLaunchTime: "[PC preview] unknown lastLaunchTime",
    isRemoved: "[PC preview] unknown isRemoved",
    installationFreeSupported: "[PC preview] unknown installationFreeSupported",
}